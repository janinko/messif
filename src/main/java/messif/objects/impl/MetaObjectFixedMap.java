/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import messif.objects.DistanceFunction;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;
import messif.objects.keys.AbstractObjectKey;
import messif.utility.ArrayMap;

/**
 * This is an implementation of the {@link MetaObject} which stores data in fixed {@link ArrayMap}.
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MetaObjectFixedMap extends MetaObject implements Serializable {

    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;    
    
    /** Map to store the data in. */
    protected final ArrayMap<String, LocalAbstractObject> objects;

    /** A distance function (can be null). */
    protected DistanceFunction<MetaObject> distance;
    
    /**
     * Create this meta object by (shallow) copying of data in given map.
     * @param objects map to copy the object from
     * @param objectKey object key
     * @param distance distance function to be used by this object (can be null)
     */
    public MetaObjectFixedMap(Map<String, LocalAbstractObject> objects, AbstractObjectKey objectKey, DistanceFunction<MetaObject> distance) {
        super(objectKey);
        this.objects = new ArrayMap(objects);
        this.distance = distance;
    }

    /**
     * Create this meta object by directly using given map.
     * @param objects map of objects (directly used)
     * @param locatorURI locator
     * @param distance distance function to be used by this object (can be null)
     */    
    protected MetaObjectFixedMap(ArrayMap<String, LocalAbstractObject> objects, String locatorURI, DistanceFunction<MetaObject> distance) {
        super(locatorURI);
        this.objects = objects;
        this.distance = distance;
    }
    
    /**
     * Create this meta object by (shallow) copying of data in given map.
     * @param objects map to copy the object from
     * @param objectKey object key
     */
    public MetaObjectFixedMap(Map<String, LocalAbstractObject> objects, AbstractObjectKey objectKey) {
        this(objects, objectKey, null);
    }
    
    /**
     * Create this meta object by (shallow) copying of data in given map.
     * @param objects map to copy the object from
     * @param locatorURI locator
     */
    public MetaObjectFixedMap(Map<String, LocalAbstractObject> objects, String locatorURI) {
        this(new ArrayMap(objects), locatorURI);
    }

    /**
     * Create this meta object by (shallow) copying of data in given arrays.
     * @param keys list of string keys
     * @param values list of {@link LocalAbstractObject}s
     * @param locatorURI locator
     */
    public MetaObjectFixedMap(String [] keys, LocalAbstractObject [] values, String locatorURI) {
        this(new ArrayMap(keys, values), locatorURI);
    }

    /**
     * Create this meta object from one key and value.
     * @param key string key
     * @param value {@link LocalAbstractObject} value
     * @param locatorURI locator
     */
    public MetaObjectFixedMap(String key, LocalAbstractObject value, String locatorURI) {
        this(key, value, locatorURI, null);
    }
    
    /**
     * Create this meta object from one key and value and distance.
     * @param key string key
     * @param value {@link LocalAbstractObject} value
     * @param locatorURI locator
     * @param distance distance function to be used by this object (can be null)
     */
    public MetaObjectFixedMap(String key, LocalAbstractObject value, String locatorURI, DistanceFunction<MetaObject> distance) {
        this(new ArrayMap(new String [] { key}, new LocalAbstractObject [] {value}, false), locatorURI, distance);
    }
    
    /**
     * Create this meta object by directly using given map.
     * @param objects map of objects (directly used)
     * @param locatorURI locator
     */    
    protected MetaObjectFixedMap(ArrayMap<String, LocalAbstractObject> objects, String locatorURI) {
        this(objects, locatorURI, null);
    }

    /**
     * Create this meta object from given existing MetaObject and one additional field (key + value).
     * If the object already contains the additional field, it is replaced.
     * 
     * @param copyObject meta object to copy all object from (and also locator)
     * @param additionalObj additional object to be added to the object
     * @param additionalKey additional field name (key)
     */    
    public MetaObjectFixedMap(MetaObject copyObject, LocalAbstractObject additionalObj, String additionalKey) {
        super(copyObject.getLocatorURI());
        
        // if the object already contains the key, replace the value
        boolean replace = copyObject.containsObject(additionalKey);
        
        String [] keys = new String [copyObject.getObjectCount() + (replace ? 0 : 1)];
        LocalAbstractObject [] values = new LocalAbstractObject [copyObject.getObjectCount() + (replace ? 0 : 1)];
        int i = 0;
        for (String key : copyObject.getObjectNames()) {
            keys[i] = key;
            values[i] = (replace && additionalKey.equals(key)) ? additionalObj : copyObject.getObject(key);
            i++;
        }
        if (! replace) {
            keys[i] = additionalKey;
            values[i] = additionalObj;
        }
        
        this.objects = new ArrayMap(keys, values, false);
        if (copyObject instanceof MetaObjectFixedMap) {
            this.distance = ((MetaObjectFixedMap) copyObject).distance;
        }
    }
    
    
    // *************       Contructors from the text representation of MESSIF 2.X     ********** //
    
    /**
     * Creates a new instance of MetaObjectMap from a text stream.
     * Only objects for names specified in <code>restrictNames</code> are added.
     * @param stream the text stream to read an object from
     * @param restrictNames if not <tt>null</tt> only the names specified in this collection are added to the objects table
     * @throws IOException when an error appears during reading from given stream,
     *         EOFException is returned if end of the given stream is reached.
     */
    public MetaObjectFixedMap(BufferedReader stream, Set<String> restrictNames, DistanceFunction<MetaObject> distance) throws IOException {
        this.objects = new ArrayMap(readObjects(stream, restrictNames, readObjectsHeader(stream), new HashMap<String, LocalAbstractObject>()));
        this.distance = distance;
    }    

    /**
     * Creates a new instance of MetaObjectMap from a text stream.
     * Only objects for names specified in <code>restrictNames</code> are added.
     * @param stream the text stream to read an object from
     * @param restrictNames if not <tt>null</tt> only the names specified in this collection are added to the objects table
     * @throws IOException when an error appears during reading from given stream,
     *         EOFException is returned if end of the given stream is reached.
     */
    public MetaObjectFixedMap(BufferedReader stream, String[] restrictNames) throws IOException {
        this(stream, (restrictNames == null)?null:new HashSet<>(Arrays.asList(restrictNames)), null);
    }

    /**
     * Creates a new instance of MetaObjectMap from a text stream.
     * @param stream the text stream to read an object from
     * @throws IOException when an error appears during reading from given stream,
     *         EOFException is returned if end of the given stream is reached.
     */
    public MetaObjectFixedMap(BufferedReader stream) throws IOException {
        this(stream, (Set<String>)null, null);
    }     

    public MetaObjectFixedMap(BufferedReader stream, DistanceFunction<MetaObject> distance) throws IOException {
        this(stream, (Set<String>)null, distance);
    }
    
    // ********************    Getters  & setters    ************************** //
    
    public void setDistance(DistanceFunction<MetaObject> distance) {
        this.distance = distance;
    }
    
    @Override
    public int getObjectCount() {
        return objects.size();
    }

    @Override
    public Collection<String> getObjectNames() {
        return objects.keySet();
    }

    @Override
    public LocalAbstractObject getObject(String name) {
        return objects.get(name);
    }

    @Override
    protected float getDistanceImpl(LocalAbstractObject obj, float[] metaDistances, float distThreshold) {
        if (distance == null) {
            throw new UnsupportedOperationException("Distance is not part of this MetaObjectFixedMap object");
        }
        if (!(obj instanceof MetaObject)) {
            throw new IllegalArgumentException("object passed to getDistanceImpl is not MetaObject: " + obj);
        }
        return distance.getDistance(this, (MetaObject) obj);
    }

    
    //****************** Text stream I/O ******************//

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        writeObjects(stream, writeObjectsHeader(stream, objects));
    }

    @Override
    public String toString() {
        return objects.toString();
    }    
}
