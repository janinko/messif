/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithms.impl;

import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationProcessor;
import messif.operations.AbstractOperation;

/**
 * An abstract base of operation processors that have only one step.
 * 
 * @author David Novak
 * @param <O> class of the operation to be processed by the specific processor
 */
public abstract class OneStepNavigationProcessor<O extends AbstractOperation> implements NavigationProcessor<O> {

    /**
     * Operation to be executed by this processor.
     */
    protected final O operation;
    
    /**
     * Flag to determine if the processing step was already performed (started).
     */
    protected boolean started = false;
    
    /**
     * Creates this processor given an algorithm and operation to be executed on it.
     * @param operation operation to be executed
     */    
    public OneStepNavigationProcessor(O operation) {
        this.operation = operation;
    }

    @Override
    public O getOperation() {
        return operation;
    }

    @Override
    public boolean isFinished() {
        return started;
    }

    @Override
    public int getProcessedCount() {
        return (started ? 0 : 1);
    }

    @Override
    public int getRemainingCount() {
        return (started ? 1 : 0);
    }
    
    @Override
    public final boolean processStep() throws AlgorithmMethodException {
        if (started) {
            return false;
        }
        started = true;
        process();
        return true;
    }
    
    /**
     * Do the one processing step.
     * @throws AlgorithmMethodException 
     */
    protected abstract void process() throws AlgorithmMethodException;
}
