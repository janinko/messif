/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.data;

import messif.buckets.BucketErrorCode;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;
import messif.operations.AbstractOperation;

/**
 * Operation for adding an object as a new field within an existing {@link MetaObject} stored in the
 *  algorithm. If the field already exists, it is either replaced or not according to a parameter.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("Add object field")
public class SetObjectFieldOperation extends DataManipulationOperation {
    
    /** Class ID for serialization. */
    private static final long serialVersionUID = 1L;

    
    //****************** Attributes ******************//

    /** An object to insert */
    private final LocalAbstractObject objectToInsert;

    /** Name of the field to add the object as */
    private final String fieldName;

    /** Locator of an existing MetaObject to which the object should be added. */
    private final String objectLocator;

    /** If true (default), the field is replaced, if exists. */
    private final boolean replaceField;
    
    //****************** Constructors ******************//

    @AbstractOperation.OperationConstructor({"object to add", "field name", "locator of existing Meta obj."})
    public SetObjectFieldOperation(LocalAbstractObject objectToInsert, String fieldName, String objectLocator) {
        this(objectToInsert, fieldName, objectLocator, true);
    }

    @AbstractOperation.OperationConstructor({"object to add", "field name", "locator of existing Meta obj.", "if true, field is replaced"})
    public SetObjectFieldOperation(LocalAbstractObject objectToInsert, String fieldName, String objectLocator, boolean replaceField) {
        this.objectToInsert = objectToInsert;
        this.fieldName = fieldName.intern();
        this.objectLocator = objectLocator;
        this.replaceField = replaceField;
    }

    //****************** Attribute access method ******************//

    public LocalAbstractObject getObjectToInsert() {
        return objectToInsert;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getObjectLocator() {
        return objectLocator;
    }

    public boolean isReplaceField() {
        return replaceField;
    }


    //****************** Overrides ******************//

    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
            case 0:
                return objectToInsert;
            case 1:
                return fieldName;
            case 2:
                return objectLocator;
            case 3:
                return replaceField;
            default:
                throw new IndexOutOfBoundsException("AddObjectField has four arguments");
        }
    }

    @Override
    public int getArgumentCount() {
        return 4;
    }

    @Override
    public boolean wasSuccessful() {
        return isErrorCode(BucketErrorCode.OBJECT_INSERTED);
    }

    @Override
    public void endOperation() {
        endOperation(BucketErrorCode.OBJECT_INSERTED);
    }

    @Override
    public void clearSurplusData() {
        super.clearSurplusData();
        objectToInsert.clearSurplusData();
    }

    //****************** Cloning ******************//

    @Override
    public SetObjectFieldOperation clone() throws CloneNotSupportedException {
        SetObjectFieldOperation operation = (SetObjectFieldOperation)super.clone();
        return operation;
    }

}
