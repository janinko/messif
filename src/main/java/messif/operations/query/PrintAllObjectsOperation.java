/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import messif.objects.AbstractObject;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.OperationErrorCode;
import messif.utility.ErrorCode;


/**
 * Operation for retrieving all objects locally stored (organized by an algorithm) and printing 
 *  their text representation to specified file.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("Get all objects query")
public class PrintAllObjectsOperation extends AbstractOperation {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Types of objects this query operation will return (print). */
    private final boolean printJustIDs;

    /** Name of the file to print the result to. */
    private final String outputFileName;

    
    // ***************   Output attribute     ********************* //
    
    /** Output stream.  */
    private OutputStream output;
    

    /****************** Constructors ******************/

    /**
     * Creates a new instance of PrintAllObjectsOperation.
     * @param printJustIDs if true, just line separated object IDs are printed
     * @param outputFileName name of text file to print the objects to; if ends with ".gz", the data is compressed
     * @throws java.io.IOException if the file cannot be created or written to
     */
    @AbstractOperation.OperationConstructor({"t/f print just IDs", "output file name"})
    public PrintAllObjectsOperation(boolean printJustIDs, String outputFileName) throws IOException {
        super();
        this.printJustIDs = printJustIDs;
        this.outputFileName = outputFileName;
        this.output = new BufferedOutputStream(new FileOutputStream(outputFileName));
        if (outputFileName.endsWith(".gz")) {
            this.output = new GZIPOutputStream(output);
        }
    }

    /**
     * Creates a new instance of PrintAllObjectsOperation to a temporary file.
     * @param printJustIDs if true, just line separated object IDs are printed
     * @throws java.io.IOException if the file cannot be created
     */
    @AbstractOperation.OperationConstructor({"t/f print just IDs"})
    public PrintAllObjectsOperation(boolean printJustIDs) throws IOException {
        this(printJustIDs, File.createTempFile("all-objects-", (printJustIDs ? ".ids" : ".data"), new File(".")).getPath());
    }
    
    /**
     * Returns argument that was passed while constructing instance.
     * If the argument is not stored within operation, <tt>null</tt> is returned.
     * @param index index of an argument passed to constructor
     * @return argument that was passed while constructing instance
     * @throws IndexOutOfBoundsException if index parameter is out of range
     */
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return printJustIDs;
        case 1:
            return outputFileName;
        default:
            throw new IndexOutOfBoundsException("GetAllObjectsQueryOperation has only two arguments");
        }
    }

    /**
     * Returns number of arguments that were passed while constructing this instance.
     * @return number of arguments that were passed while constructing this instance
     */
    @Override
    public int getArgumentCount() {
        return 2;
    }


    /******************  Creating answer (should not be in operation in the future)   ******************/

    /**
     * Print give object (or just its ID) to specified file output.
     * @param object object to print
     * @throws java.io.IOException if the write operation fails
     */
    public void print(AbstractObject object) throws IOException {
        if (printJustIDs || (!(object instanceof LocalAbstractObject))) {
            output.write(object.getLocatorURI().getBytes());
            output.write('\n');
        } else {
            ((LocalAbstractObject) object).write(output);
        }
    }
    
    
    /**
     * Prints all given objects to the specified file output.
     * 
     * @param objects the collection of objects on which to evaluate this query
     * @return number of objects printed
     * @throws java.io.IOException if the write operation fails
     */
    public int printAll(Iterator<? extends AbstractObject> objects) throws IOException {
        int count = 0;
        while (objects.hasNext()) {
            print(objects.next());
            count ++;
        }
        return count;
    }

    @Override
    public boolean wasSuccessful() {
        return getErrorCode() == OperationErrorCode.RESPONSE_RETURNED;
    }

    @Override
    public void endOperation() {
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public void endOperation(ErrorCode errValue) throws IllegalArgumentException {
        super.endOperation(errValue); //To change body of generated methods, choose Tools | Templates.
        if (output != null) {
            try {
                output.close();
            } catch (IOException ignore) {   }
        }
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    /** Pattern to strip leading zeros from a string representation of a number. */
    static Pattern STRIP_ZEROS = Pattern.compile("^0+(?!$)");
    
    /**
     * This method takes an already processed operation and prints its result as written to the
     *  output file to specified output stream.
     * @param operation an already processed operation
     * @param out output stream to print the output file to
     * @param commaSeparate if true, the results are not separated by new line but by commas
     * @param idsAsInts if true, the IDs results are to be interpreted as numbers
     * @param deleteFileAfter if true, the output file is deleted afterwards
     * @return the same operation
     */
    public static PrintAllObjectsOperation printResult(PrintAllObjectsOperation operation, PrintStream out, 
            final boolean commaSeparate, boolean idsAsInts, boolean deleteFileAfter) {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(operation.getOutputFileName()))) {
            idsAsInts = idsAsInts && operation.printJustIDs;
            while (fileReader.ready()) {
                String output = fileReader.readLine();
                if (idsAsInts) {
                    output = STRIP_ZEROS.matcher(output).replaceFirst("");
                }
                if (commaSeparate) {
                    out.print(output);
                    if (fileReader.ready()) {
                        out.print(',');
                    }
                } else {
                    out.println(output);
                }                
            }
        } catch (IOException ex) {
            out.println("error reading file " + operation.getOutputFileName());
            return operation;
        }
        if (deleteFileAfter) {
            new File(operation.getOutputFileName()).delete();
        }
        return operation;
    }
    
}
